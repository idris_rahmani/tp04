﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class MesozoicTest
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.Equals("Louis", louis.name);
            Assert.Equals("Stegausaurus", louis.specie);
            Assert.Equals(12, louis.age);
        }

        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equals("Grrr", louis.roar());
        }

        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equals("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
    }
}
